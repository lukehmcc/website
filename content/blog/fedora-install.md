+++
title = "Installing fedora on 2021 G14"
description = "The new 2021 G14 has new hardware that isn't quite fully supported. Here's how I installed Fedora 34"
date = 2021-03-26
template = "page/default.html"
draft = false
+++

---

**NOTE: This guide now exists for historical purposes**

Please follow the [New Guide Here](/wiki/fedora-guide/)

---

This guide will not cover installing fedora, partitioning, or any other parts of the basic install process.

First: in Windows open Armory Crate and find the setting which switches `iGPU` to `auto`. Otherwise dGPU will either be gone, or always on.

**Update:** I have a patch in progress to re-enable the dGPU.

## Booting the ISO

You may use Fedora 33 or 34. Replace `34` with `33` where relevant.

Fetch and write the Fedora 34 (or 33) ISO to a USB stick then reboot to the BIOS setup screen - here you need to press F7 for advanced mode then under Security turn off "Secure boot" (required for installing Nvidia drivers and the support modules). Save and exit.

Press `Esc` repeatedly when you see the POST logo, select your USB and boot. Boot will fail due to AMDGPU issues (it will lock up) so you need to edit the top boot entry as follows: select the top entry and press `e` to edit it, on the `linux` line append `nomodeset`, press the key combo to boot it.

Fedora 34 will be using LLVMPipe (cpu software renderer) to render until installed and changed back, so continue with install.

Once installed:
1. edit `/etc/default/grub` to remove `nomodeset` from the line `GRUB_CMDLINE_LINUX=`
2. run `sudo grub2-mkconfig -o /etc/grub2.cfg` to update the grub menu
3. run `sudo dnf upgrade`
4. Reboot!

Then:
1. enable [rpmfusion](https://rpmfusion.org/)
2. install the [nvidia drivers](https://rpmfusion.org/Howto/NVIDIA#Current_GeForce.2FQuadro.2FTesla)
3. run `sudo dnf install -y dkms kernel-devel`
4. create file `/etc/yum.repos.d/asus.repo` with:
```
[asus]
name=asus
failovermethod=priority
baseurl=https://download.opensuse.org/repositories/home:/luke_nukem:/asus/Fedora_33/
enabled=1
gpgcheck=0
```
5. run `sudo dnf update --repo asus --refresh && sudo dnf install asusctl dkms-hid-asus-rog dkms-asus-rog-nb-wmi`
6. run `systemctl --user enable asus-notify`
7. edit `/etc/default/grub` to remove `nvidia-drm.modeset=1` from the line `GRUB_CMDLINE_LINUX=` (you need to do this so that asusd gfx switching works)
8. run `sudo grub2-mkconfig -o /etc/grub2.cfg` to update the grub menu
9. Reboot!

## Touchpad

Touchpad will random work unless `pinctrl_amd` module is forced to load first.
Create file with this content:
```
# filename:
softdep i2c_hid pre: pinctrl_amd
softdep hid_multitouch pre: pinctrl_amd
```

**Update:** This doesn't always work. Fedora need to include the module in the kernel by default to correct the issue. You *can* build the kernel by yourself though, see the various blog posts.

## Congratulations

You should now have a functional Linux system.

## Notes:

You may want to edit `/etc/default/grub` to add `mitigations=off` to the line `GRUB_CMDLINE_LINUX=`. This turns off the various CPU attacks that came out (some of which can affect AMD) and gives a tiny performance boost.

You might also want to add a line: `GRUB_DISABLE_OS_PROBER="true"` to disable adding extra other-OS entries.

Don't forget to run `sudo grub2-mkconfig -o /etc/grub2.cfg` to update the grub menu after any edirs to `/etc/default/grub`.

You can edit most things as root with a decent GUI editor by running `sudo gedit <filename>` in a terminal.

Use `asusctl graphics --help` to switch graphics modes. `integrated` will let the dGPU turn off, while hybrid mode doesn't suspend the dGPU as it should (hardware or driver issue, not asusctl/asusd). Nvidia mode will force xorg to use the dGPU.

Wayland on Optimus laptops is a bit broken, so use xorg if you intend to use the dGPU. Nvidia *is* working on this. You will still get a very good experience using only the AMDGPU though.

Sound may or may not sound good (driver issue, in progress).