  +++
title = "Fedora 34 Setup Guide"
description = "A friendly setup guide for asusctl"
sort_by = "none"
template = "page/wiki.html"
author = "Jani Kahrama"
+++

# Fedora 34 and asusctl Setup

The recommended flavour of Linux by [asus-linux.org](http://asus-linux.org) is Fedora, for its ease of use for new users, friendly community, and good support for the latest hardware. This guide is written for Fedora 34.

This guide does not cover the choices of running Windows and Linux, or only Linux on your device, and their respective partitioning requirements.

For additional information, see the installation instructions:
[Official Fedora install guide](https://docs.fedoraproject.org/en-US/fedora/f33/install-guide/)

For simple USB stick flashing:
[Fedora Media Writer](https://getfedora.org/en/workstation/download/)

# Preparations

### Disable Secure Boot
To make sure Nvidia drivers and the necessary support modules work without issues, Secure Boot must be disabled in the UEFI.

1. Press DEL repeatedly during boot to enter UEFI setup screen
2. Press F7 for advanced mode
3. Security → Secure Boot Control → Disable
4. Save and exit

### Use the Laptop Screen
Due to display signal routing on Asus ROG laptops, and the setup process dealing with multiple graphics devices, having external screens connected during setup may result in unpredictable behavior. Please follow this guide with all external displays disconnected.

---

# Installation

1. Download Fedora 34 Workstation ISO file from [getfedora.org](https://getfedora.org/) and write it to a USB stick.
2. On booting from USB, in the Fedora boot menu, select:
Troubleshooting → Start Fedora in basic graphics mode
3. Follow the steps of the installer, and remove the USB stick when you reboot

---

# Setup

### Update the System

Before installing any additional elements, Fedora should be updated with the latest system updates.
1. Open Software store
2. Navigate to Updates tab
3. Click the Refresh-button in the top left corner
4. Download all available updates (please ignore that these screenshots are from a system set to dark desktop theme)

    ![Software Updates](/images/guide_system_download.png)

5. After the updates have been downloaded, click the "Restart & Update" button

    ![Software Updates](/images/guide_system_restart.png)

After the updates have been installed, reboot your computer.

NOTE: Upon restart the device is still possibly running in software rendering mode, symptoms:
- Laggy desktop
- Black screen after reboot
- Suspend on lid close not working
- Screen brightness control not working
- Multi-touch gestures not working


### Install Nvidia Graphics Drivers
The next step is to install the drivers for the Nvidia GPU. This involves typing *terminal commands.* To type them, start the Terminal application, which opens a window that has a command prompt.

To open the Terminal, simply press the Windows/Meta key to bring up the Activities view, and start typing "term" in the search box. Click on the search result.

![Terminal in search bar](/images/guide_terminal.png)

Commands that have *sudo* in front are administrator commands, and may require you to type in your password. You will need to use the Terminal in step 5.

1. Open Software store
2. Enable 3rd party repositories
3. Click on the hamburger menu, select Software Repositories

    ![Software repositories menu](/images/guide_repositories.png)

4. Enable RPM Fusion for Fedora 34 - Nonfree - NVIDIA Driver

    ![Nvidia repository](/images/guide_nvidia.png)

5. Input the following terminal commands:

    ```bash
    sudo dnf update -y
    sudo dnf install kernel-devel
    sudo dnf install akmod-nvidia
    sudo dnf install xorg-x11-drv-nvidia-cuda
    ```


### Install asusctl
The final step is to install asusctl, which enables controls for the Asus ROG hardware on the laptop.

1. Install asusctl

    ```bash
    sudo dnf copr enable lukenukem/asus-linux
    sudo dnf install asusctl
    sudo dnf update --refresh
    ```

2. Edit grub:

    In Terminal, launch a the "nano" text editor to edit the system boot options.

    ```bash
    sudo nano /etc/default/grub
    ```

    This will open a simple text editor. Edit the text file to look like below.

    ![Grub in nano text editor](/images/guide_grub.png)

    There are usually only two modifications to make:

    1. Change the value of `nvidia-drm.nomodeset=1` to `nvidia-drm.nomodeset=0`. This change is needed to allow switching between graphics devices without needing a reboot.
    2. Fedora installer may create duplicates of `rd.driver.blacklist=nouveau`, `modprobe.blacklist=nouveau`, and `nvidia-drm.nomodeset=1`. These duplicates can be safely deleted.

    Optionally, if the install guide has been deviated from, the same line of text that contains the previous changes may also contain a standalone word `nomodeset`. This must be removed or graphics acceleration is disabled.

    Then press `ctrl-o` to save the file, press enter.

    Finally, press `ctrl-x` to exit.

3. Update grub:

    The edits made to the grub file need to be updated to the system. This is just a single terminal command.

    ```bash
    sudo grub2-mkconfig -o /etc/grub2.cfg
    ```

4. Final step:

    Reboot the system.

    After the reboot, you should have a fully functional Fedora laptop, with ROG hardware features enabled.

Please note that asusctl is under continuous development, and will improve in capability over time.

---

# Optional Steps

### Keyboard Backlight
If the keyboard backlight does not work automatically, set a mode in asusctl:

```bash
asusctl led-mode static
```

### RGB Backlights
If an RGB capable laptop has no led modes available, then the /etc/asusd/asusd-ledmodes.toml file needs adjusting:

```bash
cat /sys/class/dmi/id/product_family # provides the prod_family var,
cat /sys/class/dmi/id/board_name # provides name to be added to board_names.
```
Edit the configuration file and add the needed values:
```bash
sudo nano /etc/asusd/asusd-ledmodes.toml
```
Again `ctrl-o` to save, `ctrl-x` to exit.

### Switching from Nvidia GPU to AMD Integrated
If the laptop has booted in Nvidia mode, switching to AMD integrated graphics and the Wayland desktop requires a reboot. To fix this so the switch only requires a logout/login, change the following:

```bash
sudo nano /lib/udev/rules.d/61-gdm.rules
```
Edit this line of text to have a hash in the front, as in the example. This will disable the setting.
```bash
# DRIVER=="nvidia", RUN+="/usr/libexec/gdm-disable-wayland"
```
Reboot to enable the change.

### Desktop Wigdets
A desktop widget to operate asusctl is currently in development:
[asusctl Gnome extension](https://gitlab.com/asus-linux/asusctl-gex)


# Known Limitations

### Nvidia Graphics on Wayland desktop
Current Nvidia drivers do not support the new Wayland desktop. This is the default desktop of Fedora 34, and works well with asusctl set to integrated mode. Whenever Nvidia graphics are enabled, in either standalone or hybrid mode, using the X.org desktop is necessary.

If Wayland desktop is started with Nvidia graphics enabled, the result may be fallback to software, a black screen, or even a crash. Using an external display with Wayland and Nvidia is likely to crash. The situation will only be improved by updated drivers from Nvidia.

To select the correct desktop environment for Nvidia, click on the gear icon in the bottom right corner of the login screen. Select "Gnome" with AMD graphics to run Wayland, or "Gnome on Xorg" when Nvidia graphics are enabled.
